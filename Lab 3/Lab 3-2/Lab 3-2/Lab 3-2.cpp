// Lab 3-2.cpp: определяет точку входа для консольного приложения.
//

#include "stdafx.h"
#include <iostream>
#include <string>
#include <fstream>

using namespace std;

int main()
{
	setlocale(LC_ALL, "Russian");

	ifstream infile;
	infile.open("text.txt");

	char str;
	string strs[100];
	int counter = 0;
	while (infile.get(str))
	{
		if (str == ' ') counter++;
		else {
			strs[counter] += str;
		}
	}

	char vowel_letter[7] = { 'а', 'о', 'у', 'ы', 'и', 'е', 'ю' };
	char latin_letter[26] = { 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z' };
	int vowel_counter = 0;
	string str_5[50];
	int str_5_counter = 0;

	string str_lat[50];
	int str_lat_counter = 0;


	ofstream output;
	output.open("output.txt");

	for (int i = 0; i <= counter; i++)
	{
		cout << strs[i] << " ";
		if (strs[i].length() < 5)
		{
			str_5[str_5_counter] = strs[i];
			str_5_counter++;
		}


		bool is_latin = false;
		for (int x = 0; x < strs[i].length(); x++)
		{
			for (int j = 0; j < 26; j++)
			{
				if (strs[i][x] == latin_letter[j])
				{
					is_latin = true;
				}
			}
		}
		for (int x = 0; x < strs[i].length(); x++)
		{
			if (is_latin == false)
			{
				output.put(strs[i][x]);
			}

			if (x == strs[i].length() - 1)
			{
				if (is_latin == false) output.put(' ');

				for (int j = 0; j < 7; j++)
				{
					if (strs[i][x] == vowel_letter[j])
					{
						vowel_counter++;
					}
				}
			}
		}
	}

	cout << "\n" << str_lat_counter;
	cout << "\nКоличество слов заканчивающихся на гласную: " << vowel_counter;
	output.write("\nКоличество слов заканчивающихся на гласную: ", strlen("\nКоличество слов заканчивающихся на гласную: "));
	output << vowel_counter;
	cout << "\n\nСлова длина которых меньше 5 символов: ";
	output.write("\n\nСлова длина которых меньше 5 символов:  ", strlen("\n\nСлова длина которых меньше 5 символов:"));

	for (int j = 0; j < str_5_counter; j++)
	{
		output.put(' ');
		cout << str_5[j] << " ";
		for (int x = 0; x < str_5[j].length(); x++)
		{
			output.put(str_5[j][x]);
		}
	}

	system("PAUSE");
	return 0;
}