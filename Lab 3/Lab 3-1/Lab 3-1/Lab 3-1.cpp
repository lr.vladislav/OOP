// Lab 3-1.cpp: определяет точку входа для консольного приложения.
//

#include "stdafx.h"
#include <iostream>

using namespace std;

class Complex {

private:
	float a;
	float b;
	float c;

public:
	Complex(float comp_a, float comp_b, float comp_c) {
		a = (comp_a), b = (comp_b), c = (comp_c);
	}

	float operator+(Complex comp1) {
		return (a + comp1.a) + (b + comp1.b) + (c + comp1.c);
	}

	float operator-(Complex comp1) {
		return (a - comp1.a) + (b - comp1.b) + (c - comp1.c);
	}

	float operator*(Complex comp1) {
		return (a * comp1.a) + (b * comp1.b) + (c * comp1.c);
	}

	float operator/(Complex comp1) {
		return (a / comp1.a) + (b / comp1.b) + (c / comp1.c);
	}
};

int main() {
	Complex comp1 = Complex(7, 12, 3);
	Complex comp2 = Complex(2, 3, 9);

	cout << comp1 + comp2 << endl;

	system("PAUSE");
	return 0;
}

