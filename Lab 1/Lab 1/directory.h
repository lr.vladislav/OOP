#pragma once

using namespace std;

class BD
{
	unsigned int count;
	char **name;
	char **surname;
	char **patronymic;
	char **address;
	char **tel;
	bool init_strs;

public:
	BD() {
		init_strs = false;

		count = 0;
	}

	BD(char* bd_name, char* bd_surname, char* bd_patronymic, char* bd_address, char* bd_tel){
		init_strs = true;

		count = 0;

		name = new char*[sizeof(bd_name)];
		surname = new char*[sizeof(bd_surname)];
		patronymic = new char*[sizeof(bd_patronymic)];
		address = new char*[sizeof(bd_address)];
		tel = new char*[sizeof(bd_tel)];

		addnumber(bd_name, bd_surname, bd_patronymic, bd_address, bd_tel);
	}

	void addnumber(char* bd_name, char* bd_surname, char* bd_patronymic, char* bd_address, char* bd_tel) {
		if (init_strs == false){
			init_strs = true;

			name = new char*[sizeof(bd_name)];
			surname = new char*[sizeof(bd_surname)];
			patronymic = new char*[sizeof(bd_patronymic)];
			address = new char*[sizeof(bd_address)];
			tel = new char*[sizeof(bd_tel)];

			name[count] = bd_name;
			surname[count] = bd_surname;
			patronymic[count] = bd_patronymic;
			address[count] = bd_address;
			tel[count] = bd_tel;
			count++;
		}
		else{
			name[count] = bd_name;
			surname[count] = bd_surname;
			patronymic[count] = bd_patronymic;
			address[count] = bd_address;
			tel[count] = bd_tel;
			count++;
		}
	}

	void show(){
		cout.width(3);
		cout << "ID" << " | ";
		cout.width(15);
		cout << "���" << " | ";
		cout.width(15);
		cout << "�������" << " | ";
		cout.width(15);
		cout << "��������" << " | ";
		cout.width(15);
		cout << "�����" << " | ";
		cout.width(15);
		cout << "�������" << " | \n\n";

		for (int i = 0; i < count; i++){
			cout.width(3);
			cout << i << " | ";
			cout.width(15);
			cout << name[i] << " | ";
			cout.width(15);
			cout << surname[i] << " | ";
			cout.width(15);
			cout << patronymic[i] << " | ";
			cout.width(15);
			cout << address[i] << " | ";
			cout.width(15);
			cout << tel[i] << " | " << endl;
		}
	}

	void show_id(int id){
		cout.width(3);
		cout << "ID" << " | ";
		cout.width(15);
		cout << "���" << " | ";
		cout.width(15);
		cout << "�������" << " | ";
		cout.width(15);
		cout << "��������" << " | ";
		cout.width(15);
		cout << "�����" << " | ";
		cout.width(15);
		cout << "�������" << " | \n\n";

		cout.width(3);
		cout << id << " | ";
		cout.width(15);
		cout << name[id] << " | ";
		cout.width(15);
		cout << surname[id] << " | ";
		cout.width(15);
		cout << patronymic[id] << " | ";
		cout.width(15);
		cout << address[id] << " | ";
		cout.width(15);
		cout << tel[id] << " | " << endl;
	}

	void delete_arr(int id){
		if (id < count){
			for (int i = id; i <= count; i++){
				name[i - 1] = name[i];
				surname[i - 1] = surname[i];
				patronymic[i - 1] = patronymic[i];
				address[i - 1] = address[i];
				tel[i - 1] = tel[i];
			}
			count--;
		}
		else if (id <= count && id > 0){
			count--;
		}
		else{
			cout << "\n������ �������� �� ����������!\n\n";
		}
	}

	void edit(int pole, int id, char* bd_val){
		switch (pole){
			case 1:{
				name[id] = bd_val;
				break;
			}
			case 2:{
				surname[id] = bd_val;
				break;
			}
			case 3:{
				patronymic[id] = bd_val;
				break;
			}
			case 4:{
				address[id] = bd_val;
				break;
			}
			case 5:{
				tel[id] = bd_val;
				break;
			}
			default:
				cout << "\n\n�� ���������� ��������\n\n";
		}
	}

	void search(char* bd_sval){
		cout.width(3);
		cout << "ID" << " | ";
		cout.width(15);
		cout << "���" << " | ";
		cout.width(15);
		cout << "�������" << " | ";
		cout.width(15);
		cout << "��������" << " | ";
		cout.width(15);
		cout << "�����" << " | ";
		cout.width(15);
		cout << "�������" << " | \n\n";

		int counter = 0;
		for (int i = 0; i < count; i++){	
			if (strcmp(bd_sval, name[i]) == 0){
				cout.width(3);
				cout << i << " | ";
				cout.width(15);
				cout << name[i] << " | ";
				cout.width(15);
				cout << surname[i] << " | " << endl;
				cout.width(15);
				cout << patronymic[i] << " | " << endl;
				cout.width(15);
				cout << address[i] << " | " << endl;
				cout.width(15);
				cout << tel[i] << " | " << endl;

				counter++;
			}
			else if (strcmp(bd_sval, surname[i]) == 0){
				cout.width(3);
				cout << i << " | ";
				cout.width(15);
				cout << name[i] << " | ";
				cout.width(15);
				cout << surname[i] << " | " << endl;
				cout.width(15);
				cout << patronymic[i] << " | " << endl;
				cout.width(15);
				cout << address[i] << " | " << endl;
				cout.width(15);
				cout << tel[i] << " | " << endl;

				counter++;
			}
			else if (strcmp(bd_sval, patronymic[i]) == 0){
				cout.width(3);
				cout << i << " | ";
				cout.width(15);
				cout << name[i] << " | ";
				cout.width(15);
				cout << surname[i] << " | " << endl;
				cout.width(15);
				cout << patronymic[i] << " | " << endl;
				cout.width(15);
				cout << address[i] << " | " << endl;
				cout.width(15);
				cout << tel[i] << " | " << endl;

				counter++;
			}
			else if (strcmp(bd_sval, address[i]) == 0){
				cout.width(3);
				cout << i << " | ";
				cout.width(15);
				cout << name[i] << " | ";
				cout.width(15);
				cout << surname[i] << " | " << endl;
				cout.width(15);
				cout << patronymic[i] << " | " << endl;
				cout.width(15);
				cout << address[i] << " | " << endl;
				cout.width(15);
				cout << tel[i] << " | " << endl;

				counter++;
			}
			else if (strcmp(bd_sval, tel[i]) == 0){
				cout.width(3);
				cout << i << " | ";
				cout.width(15);
				cout << name[i] << " | ";
				cout.width(15);
				cout << surname[i] << " | " << endl;
				cout.width(15);
				cout << patronymic[i] << " | " << endl;
				cout.width(15);
				cout << address[i] << " | " << endl;
				cout.width(15);
				cout << tel[i] << " | " << endl;

				counter++;
			}
		}
		cout << "\n������� " << counter << " ��������� �� ������ �������!\n";
	}

	void sort(int pole){
		switch (pole){
			case 1:{
				for (int i = 0; i < count; i++) {
					for (int j = i + 1; j < count; j++)
						if (strcmp(name[i], name[j]) > 0) {
							char* tmp = name[i];
							name[i] = name[j];
							name[j] = tmp;
							char* tmp1 = surname[i];
							surname[i] = surname[j];
							surname[j] = tmp1;
							char* tmp2 = patronymic[i];
							patronymic[i] = patronymic[j];
							patronymic[j] = tmp2;
							char* tmp3 = address[i];
							address[i] = address[j];
							address[j] = tmp2;
							char* tmp4 = tel[i];
							tel[i] = tel[j];
							tel[j] = tmp2;
						}
				}
				break;
			}
			case 2:{
				char* temp;
				for (int i = 0; i < count; i++) {
					for (int j = i + 1; j < count; j++)
						if (strcmp(surname[i], surname[j]) > 0) {
							char* tmp = name[i];
							name[i] = name[j];
							name[j] = tmp;
							char* tmp1 = surname[i];
							surname[i] = surname[j];
							surname[j] = tmp1;
							char* tmp2 = patronymic[i];
							patronymic[i] = patronymic[j];
							patronymic[j] = tmp2;
							char* tmp3 = address[i];
							address[i] = address[j];
							address[j] = tmp2;
							char* tmp4 = tel[i];
							tel[i] = tel[j];
							tel[j] = tmp2;
						}
				}
				break;
			}
			case 3:{
				char* temp;
				for (int i = 0; i < count; i++) {
					for (int j = i + 1; j < count; j++)
						if (strcmp(patronymic[i], patronymic[j]) > 0) {
							char* tmp = name[i];
							name[i] = name[j];
							name[j] = tmp;
							char* tmp1 = surname[i];
							surname[i] = surname[j];
							surname[j] = tmp1;
							char* tmp2 = patronymic[i];
							patronymic[i] = patronymic[j];
							patronymic[j] = tmp2;
							char* tmp3 = address[i];
							address[i] = address[j];
							address[j] = tmp2;
							char* tmp4 = tel[i];
							tel[i] = tel[j];
							tel[j] = tmp2;
						}
				}
				break;
			}
			case 4:{
				char* temp;
				for (int i = 0; i < count; i++) {
					for (int j = i + 1; j < count; j++)
						if (strcmp(address[i], address[j]) > 0) {
							char* tmp = name[i];
							name[i] = name[j];
							name[j] = tmp;
							char* tmp1 = surname[i];
							surname[i] = surname[j];
							surname[j] = tmp1;
							char* tmp2 = patronymic[i];
							patronymic[i] = patronymic[j];
							patronymic[j] = tmp2;
							char* tmp3 = address[i];
							address[i] = address[j];
							address[j] = tmp2;
							char* tmp4 = tel[i];
							tel[i] = tel[j];
							tel[j] = tmp2;
						}
				}
				break;
			}
			case 5:{
				char* temp;
				for (int i = 0; i < count; i++) {
					for (int j = i + 1; j < count; j++)
						if (strcmp(tel[i], tel[j]) > 0) {
							char* tmp = name[i];
							name[i] = name[j];
							name[j] = tmp;
							char* tmp1 = surname[i];
							surname[i] = surname[j];
							surname[j] = tmp1;
							char* tmp2 = patronymic[i];
							patronymic[i] = patronymic[j];
							patronymic[j] = tmp2;
							char* tmp3 = address[i];
							address[i] = address[j];
							address[j] = tmp2;
							char* tmp4 = tel[i];
							tel[i] = tel[j];
							tel[j] = tmp2;
						}
				}
				break;
			}
			default:
				cout << "\n\n��� ������ ����!\n\n";
		}
	}

	int get_count(){
		return count;
	}
};
