#pragma once

BD dir = BD();

void add_dir(){
	system("cls");

	if (dir.get_count() == 0){
		cout << "�� ����� - �������� ���������� ��� ������.\n\n";
	}

	cout << "���: ";
	char *name = new char[40];
	cin.width(40);
	cin >> name;

	cout << "�������: ";
	char *surname = new char[40];
	cin.width(40);
	cin >> surname;

	cout << "��������: ";
	char *patronymic = new char[40];
	cin.width(40);
	cin >> patronymic;

	cout << "�����: ";
	char *address = new char[40];
	cin.width(40);
	cin >> address;

	cout << "�������: ";
	char *tel = new char[40];
	cin.width(40);
	cin >> tel;
	cout << endl;

	dir.addnumber(name, surname, patronymic, address, tel);
}

void del_dir(){
	system("cls");

	int id = 0;
	cout << "\nID ������ �� ��������: ";
	cin >> id;
	cout << "\n\n";
	dir.delete_arr(id);
}

void edit_dir(){
	system("cls");

	int id = 0;
	int pole = 0;
	cout << "ID ���������� ������: ";
	cin >> id;
	cout << "\n���� ������� ����� ��������: ";
	cin >> pole;

	cout << "\n�������� �� ������� ����� ��������: ";
	char *val = new char[40];
	cin.width(40);
	cin >> val;

	dir.edit(pole, id, val);
	cout << endl;
}

void search_by_id(){
	system("cls");

	int id = 0;

	cout << "ID ������: "; cin >> id;
	dir.show_id(id);
}

void search(){
	system("cls");

	cout << "\n����� ��� ������: ";
	char *val = new char[40];
	cin.width(40);
	cin >> val;

	dir.search(val);
	cout << endl;
}

void sort(){
	system("cls");

	int pole = 0;
	cout << "\n���� �� �������� ��������� ����������: ";
	cin.width(1);
	cin >> pole;

	dir.sort(pole);
	cout << endl;
}

void menu(){
	if (dir.get_count() == 0){
		cout << "�� ����� - �������� ���������� ��� ������.\n\n";
		add_dir();
		menu();
	}
	else{
		int key;
		do{
			cout << "\n\n�� ������:\n1)������� ������������� ����������\n2)�������� ����������\n3)������� ����� � ���� �� id ������\n4)������� ������ �� �� id\n5)������������� ������ �� �� id\n6)������ ����� �� ���� ������\n7)����������� �� �� �������";
			cout << "\n������� ����� �������: ";
			cin.width(1);
			cin >> key;

			switch (key){
				case 1:{
					system("cls");
					dir.show();
					break;
				}
				case 2:{
					add_dir();
					break;
				}
				case 3:{
					search_by_id();
					break;
				}
				case 4:{
					del_dir();
					break;
				}
				case 5:{
					edit_dir();
					break;
				}
				case 6:{
					search();
					break;
				}
				case 7:{
					sort();
					break;
				}
				default: {
					cout << "����� ������� �� ����������!";
				}
			}
		} while (key != 0);
	}
}