#pragma once


template <class mat> class Matrix
{
private:
	mat * * matrix;
	int length;
	int count;
	int mod_sum;
public:
	Matrix(int size);

	~Matrix()
	{
		delete[] matrix;
	}

	void show_mass();
	void getmin();
	void getmod();
};

template <class mat> Matrix<mat>::Matrix(int size)
{
	length = size;
	matrix = new mat*[size];
	for (int i = 0; i < size; i++) { matrix[i] = new mat[length]; }

	int rsd = rand() % size;

	for (int i = 0; i < size; i++)
	{
		for (int j = 0; j < size; j++)
		{
			if (i == rsd)
			{
				matrix[i][j] = rand() % 20000 / 100 - 100;
			}
			else
			{
				matrix[i][j] = rand() % 10;
				if (j == size - 1) { matrix[i][j] = -10; }
			}
		}
	}
}

template <class mat>void Matrix<mat>::show_mass()
{
	for (int i = 0; i < length; i++)
	{
		for (int j = 0; j < length; j++)
		{
			cout << matrix[i][j] << " ";
		}
		cout << endl;
	}

	cout << endl << count << endl << mod_sum << endl;
}

template <class mat>void Matrix<mat>::getmin()
{
	for (int i = 0; i < length; i++)
	{
		for (int j = 0; j < length; j++)
		{
			if (matrix[i][j] == 0)
			{
				for (int j = 0; j < length; j++)
				{
					if (matrix[i][j] < 0) count++;
				}
			}
		}
	}
}

template <class mat>void Matrix<mat>::getmod()
{
	bool start = false;
	for (int i = 0; i < length; i++)
	{
		for (int j = 0; j < length; j++)
		{
			if (matrix[i][j] > 0)
			{
				start = true;
			}

			if (start == true)
			{
				mod_sum += abs(matrix[i][j]);
			}
		}
	}
}