// Lab 5.cpp: определяет точку входа для консольного приложения.
//
#include "stdafx.h"

#include <iostream>
#include <string>
#include <fstream>
#include <windows.h>


using namespace std;

#include "class.h"

int main()
{
	SetConsoleCP(1251);
	SetConsoleOutputCP(1251);

	Node<int> *Tree = new Node<int>();

	int key = 0;
	int c_key = 1;
	do
	{
		cout << "1)Добавить данные к словарю.\n2)Вывести все данные словаря.\n3)Вывести данные за ключём со словаря.\n4)Записать данные словаря.\n5)Удалить словарь.\n0)Выход!\n"; cin >> key;
		switch (key)
		{
		case 1:
		{
			string ua = ""; string en = "";
			cout << "Укр. слово: "; cin >> ua; cout << "Англ. слово: "; cin >> en;
			Tree->add_node(c_key, ua, en, Tree);
			c_key++;
			break;
		}
		case 2:
		{
			Tree->show(Tree);
			break;
		}
		case 3:
		{
			int cc_key = 0;
			cout << "\nКлюч для поиска: "; cin >> cc_key;
			Tree->show_by_id(Tree, cc_key);
			break;
		}
		case 4:
		{
			cout << endl;
			ifstream fin("dict.txt", std::ios::in);
			string str;
			int i = 0;
			string ua;
			string ch;
			while (1)
			{
				char ch1 = fin.get();
				ch = ch1;
				if (fin.eof())
					break;
				if (ch == "=")
				{
					ch = "";
					ua = str;
					str = "";
				}
				if (ch == ";")
				{
					ch = "";
					string en = str;
					Tree->add_node(c_key, en, ua, Tree);
					c_key++;
					str = "";
				}
				str += ch;
				ch = "";
			}
			break;
		}
		case 5:
		{
			Tree->del(Tree);
			break;
		}
		}
	} while (key != 0);

	cout << '\n';

	return 0;
}