#pragma once


template <typename mat> class Node
{
private:
	string ua;
	string en;
	int x;
	Node *l, *r;
public:
	Node();
	void show(Node *&Tree);
	void show_by_id(Node *&Tree, int x);
	void del(Node *&Tree);
	void add_node(int x, string ua, string en, Node *&MyTree);
};

template <class mat> Node<mat>::Node() {}

template <class mat>void Node<mat>::show(Node *&Tree)
{
	if (Tree != NULL)
	{
		show(Tree->l);
		if (Tree->x != 0)
			cout << Tree->x << ": " << Tree->en << " = " << Tree->ua << endl;
		show(Tree->r);
	}
}

template <class mat>void Node<mat>::show_by_id(Node *&Tree, int x)
{
	if (Tree != NULL)
	{
		show_by_id(Tree->l, x);
		if (Tree->x == x)
			cout << Tree->x << ": " << Tree->en << " = " << Tree->ua << endl;
		show_by_id(Tree->r, x);
	}
}

template <class mat>void Node<mat>::del(Node *&Tree)
{
	if (Tree != NULL)
	{
		del(Tree->l);
		del(Tree->r);
		delete Tree;
		Tree = NULL;
	}

}

template <class mat>void Node<mat>::add_node(int x, string ua, string en, Node *&MyTree)
{
	if (NULL == MyTree)
	{
		MyTree = new Node;
		MyTree->x = x;
		MyTree->ua = ua;
		MyTree->en = en;
		MyTree->l = MyTree->r = NULL;
	}

	if (x<MyTree->x)
	{
		if (MyTree->l != NULL) add_node(x, ua, en, MyTree->l);
		else
		{
			MyTree->l = new Node;
			MyTree->l->l = MyTree->l->r = NULL;
			MyTree->l->x = x;
			MyTree->l->ua = ua;
			MyTree->l->en = en;
		}
	}

	if (x>MyTree->x)
	{
		if (MyTree->r != NULL) add_node(x, ua, en, MyTree->r);
		else
		{
			MyTree->r = new Node;
			MyTree->r->l = MyTree->r->r = NULL;
			MyTree->r->x = x;
			MyTree->r->ua = ua;
			MyTree->r->en = en;
		}
	}
}
