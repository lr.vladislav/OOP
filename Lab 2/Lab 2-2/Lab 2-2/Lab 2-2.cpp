// Lab 2-2.cpp: определяет точку входа для консольного приложения.
//

#include "stdafx.h"

#include <windows.h>
#include <iostream>

using namespace std;

#include "painter.h"
#include "func.h"


int main()
{
	SetConsoleCP(1251);
	SetConsoleOutputCP(1251);

	set_info();
	set_color();
	draw();

	system("pause");
	return 0;
}

