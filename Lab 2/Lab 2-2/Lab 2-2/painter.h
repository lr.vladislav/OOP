#pragma once

class Point
{
protected:
	int x;
	int y;

	Point()
	{

	}
};

class Paint : public Point
{
	int radius;

	int r;
	int g;
	int b;
public:
	Paint(): Point()
	{
		r = 255;
		g = 255;
		b = 255;
	}

	void set_info(int u_x, int u_y, int u_radius)
	{
		x = u_x;
		y = u_y;
		radius = u_radius;
	}

	void set_color(int u_r, int u_g, int u_b)
	{
		r = u_r;
		g = u_g;
		b = u_b;
	}

	void draw()
	{
		HWND hWnd = GetConsoleWindow();
		HDC hDC = GetDC(hWnd);
		RECT temp;
		GetClientRect(hWnd, &temp);
		HDC hBufferDC = CreateCompatibleDC(hDC);
		HBITMAP hBufferBmp = CreateBitmap(temp.right, temp.bottom, 1, 32, NULL);
		HBITMAP hBufferBmpOld = (HBITMAP)SelectObject(hBufferDC, hBufferBmp);
		system("cls");
		HBRUSH hBrush = CreateSolidBrush(RGB(r, g, b));
		HBRUSH hOldBrush = (HBRUSH)SelectObject(hBufferDC, hBrush);
		//HPEN hOldPen = (HPEN)SelectObject(hBufferDC, hPen);

		RECT circle = { x,y,radius+x,radius+y };
		size_t step_h = 3, step_v = 3;
		while (!GetAsyncKeyState(VK_ESCAPE)) {
			if (GetAsyncKeyState(VK_LEFT)) {
				circle.left -= step_h;
				circle.right -= step_h;
			}
			if (GetAsyncKeyState(VK_RIGHT)) {
				circle.left += step_h;
				circle.right += step_h;
			}
			if (GetAsyncKeyState(VK_DOWN)) {
				circle.top += step_v;
				circle.bottom += step_v;
			}
			if (GetAsyncKeyState(VK_UP)) {
				circle.top -= step_v;
				circle.bottom -= step_v;
			}
			Ellipse(hBufferDC, circle.left, circle.top, circle.right, circle.bottom);
			BitBlt(hDC, 0, 0, temp.right, temp.bottom, hBufferDC, 0, 0, SRCCOPY);
			Sleep(10);
		}
		SelectObject(hBufferDC, hOldBrush);
		SelectObject(hBufferDC, hBufferBmpOld);
		DeleteObject(hBrush);
		DeleteObject(hBufferBmp);
		DeleteDC(hBufferDC);
		ReleaseDC(hWnd, hDC);
	}
};