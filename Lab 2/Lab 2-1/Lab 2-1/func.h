#pragma once

BD dir = BD();

void add_dir()
{
	system("cls");

	if (dir.get_count() == 0)
	{
		cout << "�� ������. �������� ���������� ��� ������.\n\n";
	}

	cout << "���: ";
	char *name = new char[40];
	cin.width(40);
	cin >> name;

	cout << "�������: ";
	char *surname = new char[40];
	cin.width(40);
	cin >> surname;

	cout << "��������: ";
	char *patronymic = new char[40];
	cin.width(40);
	cin >> patronymic;

	cout << "�����: ";
	char *address = new char[40];
	cin.width(40);
	cin >> address;

	cout << "�������: ";
	char *tel = new char[40];
	cin.width(40);
	cin >> tel;

	cout << "E-mail: ";
	char *email = new char[40];
	cin.width(40);
	cin >> email;

	cout << "�������������� �������: ";
	char *sec_tel = new char[40];
	cin.width(40);
	cin >> sec_tel;
	cout << endl;

	dir.addnumber(name, surname, patronymic, address, tel, email, sec_tel);
}

void del_dir()
{
	system("cls");

	int id = 0;
	cout << "\nID ������ �� ��������: ";
	cin >> id;
	cout << "\n\n";
	dir.delete_arr(id);
}

void menu()
{
	if (dir.get_count() == 0)
	{
		cout << "�� ����� - �������� ���������� ��� ������.\n\n";
		add_dir();
		menu();
	}
	else
	{
		int key;
		do
		{
			cout << "\n\n�� ������:\n1)������� ������������ ����������\n2)�������� ����������\n3)������� ������ �� �� id";
			cout << "\n������� ����� �������: ";
			cin.width(1);
			cin >> key;

			switch (key)
			{
				case 1:
				{
					system("cls");
					dir.show();
					break;
				}
				case 2:
				{
					add_dir();
					break;
				}
				case 3:
				{
					del_dir();
					break;
				}
				default: {
					cout << "����� ������� �� ������";
				}
			}
		} while (key != 0);
	}
}