#pragma once

using namespace std;

class Info
{
protected:
	char **name;
	char **surname;
	char **patronymic;
	char **address;
	char **tel;
	bool init_strs;
	unsigned int count;

	Info() {}

	Info(char* bd_name, char* bd_surname, char* bd_patronymic, char* bd_address, char* bd_tel) {}
};

class BD : public Info
{
	char** email;
	char** second_tel;
public:

	BD() : Info()
	{
		init_strs = false;

		count = 0;
	}

	void addnumber(char* bd_name, char* bd_surname, char* bd_patronymic, char* bd_address, char* bd_tel, char* u_email, char* u_second_tel) {
		if (init_strs == false)
		{
			init_strs = true;

			name = new char*[sizeof(bd_name)];
			surname = new char*[sizeof(bd_surname)];
			patronymic = new char*[sizeof(bd_patronymic)];
			address = new char*[sizeof(bd_address)];
			tel = new char*[sizeof(bd_tel)];
			email = new char*[sizeof(u_email)];
			second_tel = new char*[sizeof(u_second_tel)];

			name[count] = bd_name;
			surname[count] = bd_surname;
			patronymic[count] = bd_patronymic;
			address[count] = bd_address;
			tel[count] = bd_tel;
			email[count] = u_email;
			second_tel[count] = u_second_tel;
			count++;
		}
		else
		{
			name[count] = bd_name;
			surname[count] = bd_surname;
			patronymic[count] = bd_patronymic;
			address[count] = bd_address;
			tel[count] = bd_tel;
			email[count] = u_email;
			second_tel[count] = u_second_tel;
			count++;
		}
	}

	void show()
	{
		cout.width(3);
		cout << "ID" << " | ";
		cout.width(15);
		cout << "���" << " | ";
		cout.width(15);
		cout << "�������" << " | ";
		cout.width(15);
		cout << "��������" << " | ";
		cout.width(15);
		cout << "�����" << " | ";
		cout.width(15);
		cout << "�������" << " | \n\n";
		cout.width(15);
		cout << "E-mail" << " | ";
		cout.width(15);
		cout << "�������������� �������" << " | \n\n";

		for (int i = 0; i < count; i++)
		{
			cout.width(3);
			cout << i << " | ";
			cout.width(15);
			cout << name[i] << " | ";
			cout.width(15);
			cout << surname[i] << " | ";
			cout.width(15);
			cout << patronymic[i] << " | ";
			cout.width(15);
			cout << address[i] << " | ";
			cout.width(15);
			cout << tel[i] << " | ";
			cout.width(15);
			cout << email[i] << " | ";
			cout.width(15);
			cout << second_tel[i] << " | " << endl;
		}
	}

	void delete_arr(int id)
	{
		if (id < count)
		{
			for (int i = id; i <= count; i++)
			{
				name[i - 1] = name[i];
				surname[i - 1] = surname[i];
				patronymic[i - 1] = patronymic[i];
				address[i - 1] = address[i];
				tel[i - 1] = tel[i];
				email[i - 1] = email[i];
				second_tel[i - 1] = second_tel[i];
			}
			count--;
		}
		else if (id <= count && id > 0)
		{
			count--;
		}
		else
		{
			cout << "\n������ �������� �� ����������!\n\n";
		}
	}

	int get_count()
	{
		return count;
	}
};
